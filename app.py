from config import my_app

# launch local server
if __name__ == '__main__':
    my_app.run(host='localhost', port=5000, debug=True)
