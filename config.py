import connexion


# create application instance
my_app = connexion.App(__name__, specification_dir='./swagger')

my_app.add_api('swagger.yaml')
