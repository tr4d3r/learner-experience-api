from flask import Response


# base api endpoint
def main():
    response_text = '{"message":',
    '"Hello, welcome to the learner experience api"}'
    response = Response(response_text, 200, mimetype='application/json')
    return response
